create database if not exists Books;

use Books;
show tables;

create table if not exists 'Book'
(
    `id` int(11) not null auto_increment,
  `name` varchar(255) default null,
  `year` int(11) default null,
  `price` int(11) default null,
  `author_id` int(11) default null,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `Author` (`id`)

);

create table if not exists 'Author'(
    `id` int(11) not null auto_increment,
  `name` varchar(255) default null,
  'surname' varchar(255) default null,
  PRIMARY KEY (`id`)
);

create table if not exists 'Client'(
  `id` int(11) not null auto_increment,
   `name` varchar(255) default null,
  `surname` varchar(255) default null,
  `home_id` int(11) default null,
  `phone` int(11) default null,
  PRIMARY KEY (`id`),
  KEY `home_id` (`home_id`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`home_id`) REFERENCES `Home` (`id`)
);

create table if not exists 'Home'(
  `id` int(11) not null auto_increment,
  `post_index` varchar(255) default null,
  `post_index_address` varchar(255) default null,
  `post_index_phone` varchar(255) default null,
  PRIMARY KEY (`id`)
)



